% vim: set expandtab shiftwidth=2 softtabstop=2:

% generic {{{1

\language "english"

%\language "nederlands"
%\language "deutsch"

%\language "italiano"
%\language "français"

% scales {{{1

% for \modalTranspose

majorScale = { c d e f g a b }
minorScale = { a, b, c d e f g }

lydian     = { f g a b c d e }
ionian     = { c d e f g a b }
mixolydian = { g a b c d e f }
dorian     = { d e f g a b c }
aeolian    = { a b c d e f g }
phrygian   = { e f g a b c d }
locrian    = { b c d e f g a }
