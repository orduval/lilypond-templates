# vim: set expandtab shiftwidth=2 softtabstop=2:

* clefs reading

Intervals up (+) or down (-) from reference clef to another one for a
note on the same position in the staff. The octave is not taken into
account. Empty table cell means no change.

| clef         |          | reference        |                  |
|              |          | treble           | bass             |
|--------------+----------+------------------+------------------|
| french       | G1       | + 3ce            | (+2 x 8ve)       |
| treble       | G2       |                  | - 3ce (+2 x 8ve) |
| soprano      | C1       | - 3ce            | - 5te (+2 x 8ve) |
| mezzosoprano | C2       | - 5te            | + 2de (+ 8ve)    |
| alto         | C3       | + 2de (-8ve)     | - 2de (+ 8ve)    |
| tenor        | C4       | - 2de (-8ve)     | - 4te (+ 8ve)    |
| baritone     | F3 or C5 | - 4te (-8ve)     | + 3ce            |
| bass         | F4       | + 3ce (-2 x 8ve) |                  |
| subbass      | F5       | (-2 x 8ve)       | - 3ce            |
