# vim: set expandtab shiftwidth=2 softtabstop=2:

* instruments abbreviations

** voices, melodic lines

*** uppercase

| soprano                 | S |
| mezzosoprano            | M |
| alto                    | A |
| contralto, countertenor | C |
| tenor                   | T |
| baryton                 | Y |
| basse                   | B |

*** lowercase

| soprano                 | sop     |
| mezzosoprano            | mez     |
| alto                    | alt     |
| contralto, countertenor | ct, cnt |
| tenor                   | ten     |
| baritone                | bar     |
| bass                    | bs, bas |

** families

| winds     | wd  |
| woodwinds | ww  |
| brass     | br  |
| strings   | st  |
| keyboards | kbs |
| lutes     | lts |
| guitars   | gts |
| drums     | dm  |

** individuals

| flute                     | fl       |
| alto flute                | afl      |
| oboe                      | ob       |
| english horn              | eh, enhn |
| clarinet                  | cl       |
| bassoon                   | bn       |
| saxophone                 | sax      |
| bagpipe                   | bag      |
|---------------------------+----------|
| trumpet                   | tpt      |
| horn (french one)         | hn       |
| trombone                  | tbn      |
| tuba                      | tba      |
|---------------------------+----------|
| harmonica                 | hca      |
|---------------------------+----------|
| organ                     | org      |
| manual organ              | man org  |
| organ with pedal keyboard | ped org  |
| accordion                 | acc      |
| concertina                | conc     |
| bandoneon                 | band     |
|---------------------------+----------|
| violon                    | vn       |
| viola                     | va       |
| cello                     | vc       |
| contrabass                | cb       |
|---------------------------+----------|
| harp                      | hp       |
| lute                      | lt       |
| archlute                  | arlt     |
| theorbo                   | teo      |
| guitar                    | gt       |
| bass guitar               | bg       |
| sitar                     | sit      |
|---------------------------+----------|
| dulcimer                  | dul      |
|---------------------------+----------|
| harpsichord               | hpcd     |
| pedal harpsichord         | ped hpcd |
| clavichord                | clcd     |
| piano                     | pn       |
| pianoforte                | pf       |
|---------------------------+----------|
| glockenspiel              | gloc     |
| vibraphone                | vb       |
| marimba                   | mar      |
| tubular bells             | tub bel  |
| drumkit                   | dk       |
|---------------------------+----------|
| celesta                   | cel      |
|---------------------------+----------|
| keyboard                  | kb       |

* abbreviations for melody fragments

** categories

| drone (bourdon) | dr                         |
| anacrusis       | ac, anac                   |
| introduction    | in, intro                  |
| introduction to | into                       |
| coda            | co, coda, en, end, ou, out |
| theme           | tm                         |
| dual            | du, dua                    |
| polyphony       | py, pyfy, plyfny           |
| harmony         | hy, hmy, hmny              |
| cantus firmus   | cf, cnfm                   |
| counterpoint    | cp, cpt, ctpt, cnpt        |
| subject         | sj                         |
| countersubject  | cs, ctsj, cnsj             |
| answer          | an, ans                    |
| episode         | ep, epi                    |
| arpeggio        | ap, arp                    |

** details

| opened theme               | op, open                 |
| closed theme               | cl, clos                 |
|----------------------------+--------------------------|
| full measure notes         | ful                      |
| half measure notes         | haf                      |
| third measure notes        | trd, tird                |
| quarter measure notes      | qtr                      |
| sixth measure notes        | sxt                      |
| syncopation                | syn, syncop              |
| suspension                 | sus                      |
| anticipation               | ant                      |
| florid                     | flo, flor                |
|----------------------------+--------------------------|
| onbeat                     | onb                      |
| offbeat                    | ofb                      |
|----------------------------+--------------------------|
| variation                  | var                      |
| diminution                 | dim                      |
| augmentation               | aug                      |
| retrograde                 | ret                      |
| mirror (up and down)       | mir                      |
| inversion (backward)       | inv                      |
| modulation                 | mod                      |
|----------------------------+--------------------------|
| part                       | pt                       |
| shifted in time            | shift                    |
| strette                    | stret                    |
|----------------------------+--------------------------|
| mixed                      | mix                      |
|----------------------------+--------------------------|
| contains an intro to THEME | SOMETHING_in_THEME       |
|                            | SOMETHING_into_THEME     |
| adapted to a theme         | something_adap_THEME     |
| intended for INSTRUMENT    | something_for_INSTRUMENT |

* vim marks

** qwerty

| flute                    | f |
| oboe                     | t |
| clarinet                 | r |
| english horn             | e |
| bassoon                  | w |
|--------------------------+---|
| trumpet                  | u |
| french horn              | y |
|--------------------------+---|
| manual organ, right      | o |
| manual organ, left       | p |
|--------------------------+---|
| pedal organ, right       | i |
| pedal organ, left        | o |
| pedal organ, feet        | p |
|--------------------------+---|
| string quartet, soprano  | z |
| string quartet, alto     | x |
| string quartet, ténor    | c |
| string quartet, basse    | v |
|--------------------------+---|
| string quintet, soprano  | a |
| string quintet, mezzo    | z |
| string quintet, alto     | x |
| string quintet, ténor    | c |
| string quintet, basse    | v |
|--------------------------+---|
| string sextet, soprano   | a |
| string sextet, mezzo     | s |
| string sextet, alto      | z |
| string sextet, ténor     | x |
| string sextet, baritone  | c |
| string sextet, basse     | v |
|--------------------------+---|
| string septet, soprano   | a |
| string septet, mezzo     | s |
| string septet, alto      | d |
| string septet, counter   | z |
| string septet, ténor     | x |
| string septet, baryton   | c |
| string septet, basse     | v |
|--------------------------+---|
| guitar                   | g |
| bass guitar              | b |
|--------------------------+---|
| lute                     | g |
| theorbo                  | b |
|--------------------------+---|
| harp, upper              | h |
| harp, lower              | n |
|--------------------------+---|
| harpsichord, right       | k |
| harpsichord, left        | l |
|--------------------------+---|
| pedal harpsichord, right | j |
| pedal harpsichord, left  | k |
| pedal harpsichord, feet  | l |
|--------------------------+---|
| piano, right             | k |
| piano, left              | l |

** azerty

| flute                    | f |
| oboe                     | t |
| clarinet                 | r |
| english horn             | e |
| bassoon                  | z |
|--------------------------+---|
| trumpet                  | u |
| french horn              | y |
|--------------------------+---|
| manual organ, right      | o |
| manual organ, left       | p |
|--------------------------+---|
| pedal organ, right       | i |
| pedal organ, left        | o |
| pedal organ, feet        | p |
|--------------------------+---|
| string quartet, soprano  | w |
| string quartet, alto     | x |
| string quartet, ténor    | c |
| string quartet, basse    | v |
|--------------------------+---|
| string quintet, soprano  | q |
| string quintet, mezzo    | w |
| string quintet, alto     | x |
| string quintet, ténor    | c |
| string quintet, basse    | v |
|--------------------------+---|
| string sextet, soprano   | q |
| string sextet, mezzo     | s |
| string sextet, alto      | w |
| string sextet, ténor     | x |
| string sextet, baritone  | c |
| string sextet, basse     | v |
|--------------------------+---|
| string septet, soprano   | q |
| string septet, mezzo     | s |
| string septet, alto      | d |
| string septet, counter   | w |
| string septet, ténor     | x |
| string septet, baryton   | c |
| string septet, basse     | v |
|--------------------------+---|
| guitar                   | g |
| bass guitar              | b |
|--------------------------+---|
| lute                     | g |
| theorbo                  | b |
|--------------------------+---|
| harp, upper              | h |
| harp, lower              | n |
|--------------------------+---|
| harpsichord, right       | k |
| harpsichord, left        | l |
|--------------------------+---|
| pedal harpsichord, right | j |
| pedal harpsichord, left  | k |
| pedal harpsichord, feet  | l |
|--------------------------+---|
| piano, right             | k |
| piano, left              | l |
