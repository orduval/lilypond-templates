% vim: set expandtab shiftwidth=2 softtabstop=2 cms=%%s:

* Atmospheres

| Instrument  | Suggestion                   |
|-------------+------------------------------|
| flute       | sunshine                     |
| harpsichord | sunshine sparks on the water |
| harp        | dew                          |
