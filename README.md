# Introduction

Some lilypond templates files for music bands or small orchestras. All
is folded with Vim markers for an easier navigation.

# Meta orchestra

The most important file is
[meta.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/meta.ly?ref_type=heads).
As its name suggests, it's a meta-orchestra that contains
various instruments and groups. You can use it to build
a custom orchestra, by uncommenting or commenting them as
needed, in the "book" section, at the end of the file. The file
[neutral.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/neutral.ly?ref_type=heads)
is the same, but all instruments and groups are commented, waiting to
be enabled.

The melodic themes can be inserted in the "voice" variables. For instance,
"voicePianoRight" contains the music of the right hand on the piano.

In the case of a group covering a wide pitch range, the voices are
labelled from soprano (the highest) to bass (the lowest). So, for
a string quintet, it will be : 

- voiceStringQuintetSoprano
- voiceStringQuintetMezzo
- voiceStringQuintetAlto
- voiceStringQuintetTenor
- voiceStringQuintetBass

Here is the list of instruments and groups contained in this file :

- vocal music
  + individual voices
    - soprano
    - mezzo
    - alto
    - countertenor
    - tenor
    - barytone
    - bass
  + vocal quartet
    - soprano alto tenor bass
  + vocal quintet 
    - soprano mezzo alto tenor bass
  + vocal sextet
    - soprano mezzo alto tenor barytone bass
  + vocal septet
    - soprano mezzo alto countertenor tenor barytone bass
- woodwinds
  + pairs of individual woodwinds
    - flutes
      + flute
      + alto flute
    - 2 oboes
    - 2 clarinets
    - 2 english horns
    - 2 bassoons
    - 2 sax soprano
    - 2 sax alto
    - 2 sax tenor
    - 2 sax barytone
    - bagpipe
  + woodwind quartet
    - flute
    - oboe
    - clarinet
    - bassoon
  + woodwind quintet
    - flute
    - oboe
    - clarinet
    - english horn
    - bassoon
  + woodwind sextet
    - flute
    - alto flute 
    - oboe
    - clarinet
    - english horn
    - bassoon
  + saxophones quartet
- brass
  + pairs of individual brass
    - 2 trumpets
    - 2 french horns
    - 2 trombones
    - 2 tubas
  + brass quartet
    - 2 trumpets
    - 2 french horns
  + brass quintet
    - 2 trumpets
    - 1 french horns
    - 1 trombone
    - 1 tuba
- free reeds
  + harmonica
- keyboard winds
  + organ without pedal keyboard
    - positive organ, reed organ, hammond organ
  + organ with pedal keyboard
    - positive organ, reed organ, hammond organ, church organ
  + accordion
  + concertina
- bowed strings
  + pairs of individual strings
    - 2 violins
    - 2 violas
    - 2 cellos
    - 2 contrabasses
  + string quartet
    - 2 violins
    - 1 viola
    - 1 cello
  + string quartet, symmetric
    - 2 violins
    - 2 cellos
  + string quartet with contrabass
    - violin
    - viola
    - cello
    - contrabass
  + string quintet
    - 2 violins
    - 2 violas
    - 1 cellos
  + string quintet, symmetric
    - 2 violins
    - 1 violas
    - 2 cellos
  + string quintet with contrabass
    - 2 violins
    - 1 viola
    - 1 cello
    - 1 contrabass
  + string sextet
    - 2 violins
    - 2 violas
    - 2 cellos
  + string septet
    - 2 violins
    - 2 violas
    - 2 cellos
    - 1 contrabass
- plucked strings
  + harp
  + lute
    - ordinary staff 
    - tablature
  + archlute
    - ordinary staff 
    - tablature
  + theorbo
    - ordinary staff 
    - tablature
  + guitar
    - ordinary staff 
    - tablature
  + bass guitar
    - ordinary staff 
    - tablature
  + sitar
- hammered strings
  + dulcimer
- keyboard strings
  + viola organista
    - keyboard with strings rubbed by wheels
  + harpsichord
  + harpsichord with pedal keyboard
  + clavichord
  + piano
- drums
  + glockenspiel
  + vibraphone
  + marimba
  + tubular bells
  + drumkit
- keyboard drums
  + celesta

These instruments and groups are arranged as much as possible with two
melodies per staff, to get a more compact score.

The individual instruments are named like that because it allows you to
enable only one kind of instrument. Some of them are in fact available by
pairs: flutes, oboes, trumpets, trombones, violins, cellos, and so on.
Each pair is then arranged as two melodies on a same staff. If you need
only one of them, you can fill the first voice variable (for instance
voiceFluteOne for the first flute) and leave the second empty. To get
the usual representation of notes on a single voice staff, just insert
\oneVoice at the beginning of the first voice variable.

# Templates list

The other files are derived from meta, as examples of applications. They
contain small orchestra templates :

- [neutral.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/neutral.ly?ref_type=heads) : empty, all is commented, waiting to be activated
- [vocal-quartet.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/vocal-quartet.ly?ref_type=heads)
- [vocal-quintet.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/vocal-quintet.ly?ref_type=heads)
- [vocal-sextet.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/vocal-sextet.ly?ref_type=heads)
- [vocal-septet.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/vocal-septet.ly?ref_type=heads)
- [winds.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/winds.ly?ref_type=heads)
- [woodwinds.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/woodwinds.ly?ref_type=heads)
- [brass.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/brass.ly?ref_type=heads)
- [strings.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/strings.ly?ref_type=heads)
- [string-quartet.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/string-quartet.ly?ref_type=heads) : 2 violins, viola, cello
- [string-quartet-symmetric.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/string-quartet-symmetric.ly?ref_type=heads) : 2 violins, 2 cellos
- [string-quartet-contrabass.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/string-quartet-contrabass.ly?ref_type=heads) : violin, viola, cello, contrabass
- [string-quintet.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/string-quintet.ly?ref_type=heads) : 2 violins, 2 violas, cello
- [string-quintet-symmetric.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/string-quintet-symmetric.ly?ref_type=heads) : 2 violins, viola, 2 cellos
- [string-quintet-contrabass.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/string-quintet-contrabass.ly?ref_type=heads) : 2 violins, viola, cello, contrabass
- [string-sextet.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/string-sextet.ly?ref_type=heads)
- [string-septet.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/string-septet.ly?ref_type=heads)
- [piano-quintet.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/piano-quintet.ly?ref_type=heads)
- [keyboards.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/keyboards.ly?ref_type=heads)
- [drums.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/drums.ly?ref_type=heads)
- [boudoir.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/boudoir.ly?ref_type=heads) : flutes, string quartet, keyboard
- [chamber.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/chamber.ly?ref_type=heads)
- [jazz.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/jazz.ly?ref_type=heads)

# Included files

- [global/global.glob.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/global/global.glob.ly?ref_type=heads) : global variables
- melodic themes, hypermeasure 2x4
  + [melody/mel-2-2-4.mld.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/melody/mel-2-2-4.mld.ly?ref_type=heads) : time 2/4
  + [melody/mel-3-2-4.mld.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/melody/mel-3-2-4.mld.ly?ref_type=heads) : time 3/4
  + [melody/mel-4-2-4.mld.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/melody/mel-4-2-4.mld.ly?ref_type=heads) : time 4/4
- melodic themes, hypermeasure 3x2
  + [melody/mel-2-3-2.mld.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/melody/mel-2-3-2.mld.ly?ref_type=heads) : time 2/4
  + [melody/mel-3-3-2.mld.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/melody/mel-3-3-2.mld.ly?ref_type=heads) : time 3/4
  + [melody/mel-4-3-2.mld.ly](https://gitlab.com/orduval/lilypond-templates/-/blob/main/melody/mel-4-3-2.mld.ly?ref_type=heads) : time 4/4
