# vim: set ft=make :

.PHONY: all install update pdf midi ogg mp3 clean wipe
.PRECIOUS: %.midi

.DEFAULT_GOAL := all

RSYNC_EXCLUDE := rsync -au --delete --exclude-from=./rsync-exclude -vhi --progress
RSYNC := rsync -au --delete -vhi --progress

# wildcards {{{1

lilypond_files = $(wildcard *.ly)
global_files   = $(wildcard global/*.ly)
melody_files   = $(wildcard melody/*.ly)
pdf_files      = $(lilypond_files:.ly=.pdf)
png_files      = $(lilypond_files:.ly=.png)
midi_files     = $(lilypond_files:.ly=.midi)
ogg_files      = $(lilypond_files:.ly=.ogg)
mp3_files      = $(lilypond_files:.ly=.mp3)

# pdf, midi {{{1

neutral.pdf neutral.midi: neutral.ly
	# neutral is empty, not expected to produce any output
	touch neutral.pdf neutral.midi

%.pdf %.midi: %.ly $(melody_files) $(global_files)
	lilypond -ddelete-intermediate-files -dno-point-and-click $<
	@echo
	@echo "------------"
	@echo

# ogg {{{1

neutral.ogg: neutral.ly
	# neutral is empty, not expected to produce any output
	touch neutral.ogg

%.ogg: %.midi
	timidity-ogg.zsh $<
	#fluidsynth-ogg.zsh $<
	sox $*.ogg $*-pad.ogg pad 1 1
	mv -f $*-pad.ogg $*.ogg
	@echo
	@echo "------------"
	@echo

# mp3 {{{1

neutral.mp3: neutral.ly
	# neutral is empty, not expected to produce any output
	touch neutral.mp3

%.mp3: %.midi
	timidity-mp3.zsh $<
	#fluidsynth-mp3.zsh $<
	sox $*.mp3 $*-pad.mp3 pad 1 1
	mv -f $*-pad.mp3 $*.mp3
	@echo
	@echo "------------"
	@echo

# png {{{1

neutral.png: neutral.ly
	# neutral is empty, not expected to produce any output
	touch neutral.png

%.png: %.pdf
	pdftoppm $< $* -png
	mv -f $*-1.png $*.png || echo not 1, 01 maybe
	mv -f $*-01.png $*.png || echo not 01
	@echo
	@echo "------------"
	@echo

# filetypes {{{1

pdf: $(pdf_files)
midi: $(midi_files)
ogg: $(ogg_files)
mp3: $(mp3_files)

# install {{{1

install: $(png_files)
	$(RSYNC_EXCLUDE) . ~/racine/site/eclats2vers/generic/musica/lilypond/template
	$(RSYNC) *.png ~/racine/site/eclats2vers/generic/image/musica/template
	@echo
	@echo "------------"
	@echo

# update {{{1

update:
	convert-ly -e *.ly

# clean {{{1

clean:
	rm -vf *~
	#rm -vf -- *-1.png
	#rm -vf -- *-2.png
	#rm -vf ~/racine/musica/audio/*-frag.ogg
	@echo
	@echo "------------"
	@echo

# wipe {{{1

wipe:
	rm -vf *~ *.midi *.pdf *.ogg *.mp3 *.png
	@echo
	@echo "------------"
	@echo

# all {{{1

#all: $(pdf_files) $(midi_files) $(ogg_files) $(mp3_files) $(png_files) install clean
all: install
